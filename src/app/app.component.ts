import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApicallfunProvider } from '../providers/apicall';
import { ApicallProvider } from '../providers/api';
import { Http } from '@angular/http';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  private baseURl = ApicallProvider.baseURl;
  shownGroup = null;
  rootPage: any = "LoginPage";

  pages: Array<{title: string, component: any}>;
  category: any;
  data: any;
  subcategory: any[];
  id: any;
  category_id: any;
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private api: ApicallfunProvider, private http: Http, public events: Events, public loadingCtrl: LoadingController) {
    this.initializeApp();
    this.id = setInterval(() => {
      this.loggedIn(); 
    }, 2000);
    // used for an example of ngFor and navigation

  }
  loggedIn(){
    if(localStorage.getItem("iobasket")){
    this.data = JSON.parse(localStorage.getItem("iobasket"));
    console.log(this.data);
    this.getslider(this.data.id);
    clearInterval(this.id);
    }
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  contactusPage() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push("ContactusPage");
  }

  async getslider(id) {
    var objsldr = {
      "user_id":id
    }
    console.log(objsldr);
    this.http.post(this.baseURl+"category", objsldr).subscribe(data => {
      console.log(data.json());
      if(data.json().status === '1'){
        this.category = data.json().category;
      }
    }, (err) => {
      console.log(err);
    });
  }
  isGroupShown(group) {
    return this.shownGroup === group;
};
  async toggleSection(i, id) {
    console.log(i);
    console.log(id);
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    this.category[i].open = !this.category[i].open;  
    this.subcategory = []; 
    if (this.isGroupShown(i)) {
      this.shownGroup = null;
  } else {
      this.shownGroup = i;
  }
    var objsldr = {
      "user_id":this.data.id,
      "category_id":id
    }
    this.category_id = id
    console.log(objsldr);
    if(this.category[i].open){
      
    this.http.post(this.baseURl+"subcategory", objsldr).subscribe(data => {
      console.log(data.json());
      if(data.json().status === '1'){
        this.subcategory = data.json().subcategory;
        console.log(this.subcategory);
        loading.dismiss();
      }
    }, (err) => {
      console.log(err);
      loading.dismiss();
    });
    }
    else{  
      this.subcategory = [];
      loading.dismiss();
    }
  }

  async toggleItem(i, j, id) {
    this.subcategory = [];
    var objsldr = {
      "user_id":this.data.id,
      "category_id":id
    }
    console.log(objsldr);
    
    let subcategory = await this.api.getsubcategory(objsldr);
    console.log(subcategory);
    if(subcategory.status==="1"){
      this.subcategory = subcategory.subcategory;
    }
    else{
      
    }
  }

  productPage(subcategory_id, subcategory_name){
    console.log(this.data.id);
    this.nav.push("ProductPage",{
      user_id: this.data.id,
      subcategory_id: subcategory_id,
      subcategory_name: subcategory_name
    });
  }

  logout(){
    localStorage.removeItem("iobasket");
    localStorage.removeItem("iobasketUsrImg");
    this.nav.setRoot("LoginPage");
  }
}
