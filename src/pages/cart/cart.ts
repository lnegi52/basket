import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, LoadingController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ApicallfunProvider } from '../../providers/apicall';
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  data: any = {};
  product: any = [];
  prices: any = [];
  product_ids: any = [];
  quantities: any = [];
  imageUrl = "http://store.thebusinessaroma.com.thebusinessaroma.com/webroot/img/product_image/"
  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private alertCtrl: AlertController, public loadingCtrl: LoadingController, private api: ApicallfunProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage'); 
    this.data = JSON.parse(localStorage.getItem("iobasket")); 
    this.platform.ready().then(() => {
      this.getCartData();
    })
  }

  getCartData(){
    //call openDB method
this.sqlite.create({
  name: 'ioshopdata.db',
  location: 'default'
}).then((db: SQLiteObject) => {

  db.executeSql("SELECT * FROM cartllist", [])
  .then(res => {
    console.log(res.rows.item(0))
        if(res.rows.length>0) {
          console.log(res)
          for(var x=0; x<res.rows.length; x++){
            this.product.push(res.rows.item(x));
            console.log(this.product)
          }
        }
      })

    })
  }

  deleteData(rowid) {
    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM cartllist WHERE id=?', [rowid])
      .then(res => {
        console.log(res);
        this.product = [];
        this.getCartData();
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  qtyplus(id, quantity){
    quantity = quantity+1
    // this.product[id].quantity = quantity;

    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('UPDATE cartllist SET quantity=? WHERE id=?',[quantity, id])
      .then(res => {
        this.product = [];
        console.log(res);        
        this.getCartData();
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));

  }
  qtyminus(id, quantity){
    if(quantity>1){
      quantity = quantity-1
      // this.product[id].quantity = quantity;
      this.sqlite.create({
        name: 'ioshopdata.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('UPDATE cartllist SET quantity=? WHERE id=?',[quantity, id])
        .then(res => {
          this.product = [];
          console.log(res);
          this.getCartData();
        })
        .catch(e => console.log(e));
      }).catch(e => console.log(e));

    }
  }

  placeorder(){
      let alert = this.alertCtrl.create({
        title: 'Confirm purchase',
        message: 'Do you want to placeorder?',
        buttons: [
          {
            text: 'CANCEL',
            role: 'cancel',
            handler: () => {
              console.log('Cancel');
            }
          },
          {
            text: 'ORDER NOW',
            handler: () => {
              console.log('Buy');
              this.goBuy();
            }
          }
        ]
      });
      alert.present();
  }

  async goBuy() {
    console.log(this.product);
    for(var x=0; x<this.product.length; x++){
      this.product_ids[x] = {"product_id":this.product[x].prod_id}
      this.quantities[x] = {"quantity":this.product[x].quantity}
      this.prices[x] = {"price":this.product[x].price*this.product[x].quantity}
    }
    console.log(this.product_ids);
    console.log(this.quantities);
    console.log(this.prices);
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    var objsldr = {
      "user_id":this.data.id,
      "product_id":JSON.stringify(this.product_ids),
      "quantity":JSON.stringify(this.quantities),
      "price":JSON.stringify(this.prices)
    }
    console.log(JSON.stringify(objsldr));
    
    let orderpla = await this.api.getplaceorder(objsldr);
    console.log(orderpla);
    if(orderpla.status==="1"){
      loading.dismiss();
      alert("Thanks for placing an order with Meli Ram Yashpal Jain Store.");
      let ordersms = await this.api.getotp(orderpla.sms_url);
        this.sqlite.create({
          name: 'ioshopdata.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql('DELETE FROM cartllist', [])
          .then(res => {
            console.log(res);
            this.product = [];
            this.getCartData();
          })
          .catch(e => console.log(e));
        }).catch(e => console.log(e));

      this.navCtrl.setRoot("HomePage");
    }
    else{
      loading.dismiss();
    }
  }

  continueShop(){
    this.navCtrl.setRoot("HomePage");
  }
}
