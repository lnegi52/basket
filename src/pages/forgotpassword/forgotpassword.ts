import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicallfunProvider } from '../../providers/apicall';

/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
  otpForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private api: ApicallfunProvider) {
    this.otpForm = this.formBuilder.group({
      email: ['']
    });
  }

  ionViewDidLoad() {
  }

  async submitOtp(email) {
    if(email == undefined || email == null || email == ""){
      alert("Please provide OTP for verification");
    }
    else{
      var objsldr = {
        "email":email
      }
        let otpVerify = await this.api.getforgotpass(objsldr);
        console.log(otpVerify);
        if(otpVerify.status==="1"){
          console.log(otpVerify);      
          let otpUrl = await this.api.getotp(otpVerify.url);
          if(otpUrl.status==="OK"){
            alert(otpUrl.message);
            this.navCtrl.setRoot("OtpPage");
          }
          this.navCtrl.setRoot("LoginPage");
        }
      else if(otpVerify.status==="5"){  
        alert("Invalid User email id")
      }
      else{
        alert(otpVerify.message);
      }
    }
  }
}
