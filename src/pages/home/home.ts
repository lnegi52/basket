import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApicallfunProvider } from '../../providers/apicall';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  data: any={};
  slider: any;
  category: any;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, private api: ApicallfunProvider) {
  }
  ionViewWillEnter(){
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    console.log(this.category);
    this.data = JSON.parse(localStorage.getItem("iobasket"));
    console.log(this.data);
    this.getslider(this.data.id);
  }

  subcategoryPage(category_id, category_name){
    this.navCtrl.push("Home2Page",{
      user_id: this.data.id,
      category_id: category_id,
      category_name: category_name
    })
  }
  productPage(){
    this.navCtrl.push("ProductPage");
  }

  goCart(){
    this.navCtrl.push("CartPage");
  }

  goProfile(){
    this.navCtrl.push("ProfilePage");
  }

  async getslider(id) {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    var objsldr = {
      "user_id":id
    }
    console.log(objsldr);
    let sliders = await this.api.getslider(objsldr);
    console.log(sliders);
    if(sliders.status==="1"){
      this.slider = sliders;
      loading.dismiss();
    }
    
    let category = await this.api.getcategory(objsldr);
    console.log(category);
    if(category.status==="1"){
      this.category = category.category;
      for(var x=1; x<this.category.length; x++){
        if( this.category[x-1].id == x){
          this.category[0].image = "assets/image/"+0+".jpg"
          this.category[x].image = "assets/image/"+x+".jpg"
        }
      }
    }
    else{
      loading.dismiss();
    }
  }

  openSearch(){
    this.navCtrl.push("SearchPage");
  }
}
