import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApicallfunProvider } from '../../providers/apicall';

/**
 * Generated class for the Home2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home2',
  templateUrl: 'home2.html',
})
export class Home2Page {
  subcategory: any;
  category_name: any;
  data: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, private api: ApicallfunProvider, public loadingCtrl: LoadingController) {
    let user_id = navParams.get("user_id");
    this.data.id = user_id;
    let category_id = navParams.get("category_id");
    this.category_name = navParams.get("category_name");
    console.log(this.category_name);
    this.getslider(user_id, category_id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home2Page');
  }
  async getslider(user_id, category_id) {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    var objsldr = {
      "user_id":user_id,
      "category_id":category_id
    }
    console.log(objsldr);
    
    let subcategory = await this.api.getsubcategory(objsldr);
    console.log(subcategory);
    if(subcategory.status==="1"){
      loading.dismiss();
      this.subcategory = subcategory.subcategory;
      for(var x=0; x<this.subcategory.length; x++){
          this.subcategory[x].image =  "assets/image/subcat/"+this.subcategory[x].id+".jpg"
      }
    }
  }
  
  productPage(subcategory_id, subcategory_name){
    console.log(this.data.id);
    this.navCtrl.push("ProductPage",{
      user_id: this.data.id,
      subcategory_id: subcategory_id,
      subcategory_name: subcategory_name
    });
  }
}
