import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicallfunProvider } from '../../providers/apicall';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  credentialsForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public formBuilder: FormBuilder, private api: ApicallfunProvider, public menu: MenuController, public events: Events, private toastCtrl: ToastController) {
    this.credentialsForm = this.formBuilder.group({
      email: [''],
      password: ['']
    });
  }

  presentToast(user) {
    let toast = this.toastCtrl.create({
      message: "Hi "+user+", Welcome to Meli Ram Store",
      duration: 5000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  ionViewWillEnter(){
    let iobasket = localStorage.getItem("iobasket");
    let iobasketUsrImg = localStorage.getItem("iobasket");
    if(iobasket != undefined && iobasketUsrImg != undefined){
      this.navCtrl.setRoot("HomePage");
    }
    this.menu.enable(false);
  }
  ionViewWillLeave(){
    this.menu.enable(true);
  }

  onSignup(){
    this.navCtrl.push("SignupPage");
  }

  onForgotPassword() {
    this.navCtrl.push("ForgotpasswordPage");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async onSignIn() {
    if(this.credentialsForm.value.email==""||this.credentialsForm.value.password==""){
      alert("Email and Password both are mandatory fields");
    }
    else{
      let loading = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Please Wait...'
      });
      loading.present();
      let login = await this.api.getlogin(this.credentialsForm.value);
      console.log(login);
      if(login.status==="1"){
        loading.dismiss();
        this.events.publish('user:login');
        localStorage.setItem("iobasket",JSON.stringify(login.user_data));
        localStorage.setItem("iobasketUsrImg",JSON.stringify(login.image_url));
        this.presentToast(login.user_data.name);
        this.navCtrl.setRoot("HomePage");
      }
      else if(login.status==="0"){
        loading.dismiss();
        alert(login.message);
      }
    }
  }
}
