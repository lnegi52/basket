import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicallfunProvider } from '../../providers/apicall';
/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  otpForm: FormGroup;
  emailForm: FormGroup;
  iootpId: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private api: ApicallfunProvider) {
    this.emailForm = this.formBuilder.group({
      email: ['']
    });
    this.otpForm = this.formBuilder.group({
      otp: ['']
    });
  }

  ionViewDidLoad() {
    this.iootpId = localStorage.getItem("iootpId");
    console.log('ionViewDidLoad OtpPage');
  }

  async submitOtp(otp_key) {
    if(otp_key == undefined || otp_key == null || otp_key == ""){
      alert("Please provide OTP for verification");
    }
    else{
      var objsldr = {
        "user_id":this.iootpId,
        "otp_key":otp_key
      }
        let otpVerify = await this.api.getverifyotp(objsldr);
        console.log(otpVerify);
        if(otpVerify.status==="1"){
          console.log(otpVerify);
          alert(otpVerify.message);
          this.navCtrl.setRoot("LoginPage");
        }
      else{  
      }
    }
  }
}
