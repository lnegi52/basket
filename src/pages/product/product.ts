import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ApicallfunProvider } from '../../providers/apicall';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  category: any;
  data: any={};
  subcategory_name: any;
  product: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private api: ApicallfunProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private sqlite: SQLite) {
    this.data.id = navParams.get("user_id");
    let subcategory_id = navParams.get("subcategory_id");
    this.subcategory_name = navParams.get("subcategory_name");
    console.log(this.subcategory_name);
    this.getproduct(this.data.id, subcategory_id);
    if(localStorage.getItem("iocart")==undefined){
      localStorage.setItem("iocart","");
    }
  }
  ionViewWillEnter(){
    this.getCartlength();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  getCartlength(){

    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
    
      db.executeSql("SELECT * FROM cartllist", [])
      .then(res => {
        console.log("Selected Data from db"+res)
            if(res.rows.length>0) {
              this.data.cartlength = res.rows.length
              }
              else{
                this.data.cartlength = 0;
              }
          })

        })

  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  // qtyplus(id, quantity){
  //   console.log(id);
  //   console.log(quantity);
  //     quantity = quantity+1
  //     this.product[id].quantity = quantity;
  // }
  // qtyminus(id, quantity){
  //   console.log(id);
  //   console.log(quantity);
  //   if(parseInt(quantity)>1){
  //     quantity = quantity-1
  //     this.product[id].quantity = quantity;
  //   }
  // }


  qtyplus(id, quantity){
    quantity = quantity+1
    this.product[id].quantity = quantity;
  }
  qtyminus(id, quantity){
    if(quantity>1){
      quantity = quantity-1
      this.product[id].quantity = quantity;
    }
  }

  async getproduct(user_id, subcategory_id) {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    var objsldr = {
      "user_id":user_id,
      "subcategory_id":subcategory_id
    }
    console.log(objsldr);
    
    let product = await this.api.getproduct(objsldr);
    console.log(product);
    if(product.status==="1"){
      loading.dismiss();
      this.product = product.product;
      this.data.prod_image_url = product.prod_image_url;
      this.data.quantity = 1;
      for(var x = 0; x<this.product.length; x++ ){
        this.product[x].quantity = 1
        this.product[x].price = parseInt(this.product[x].price)
      }
    }
  }

  addtocart(id, data){
    console.log(id);
    console.log(data);

    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
      })
      .then((db: SQLiteObject) => {
      
      //create table section
      db.executeSql('CREATE TABLE IF NOT EXISTS cartllist(id INTEGER PRIMARY KEY AUTOINCREMENT, prod_id INTEGER, name VARCHAR(32), price INTEGER, quantity INTEGER, company VARCHAR(32), image VARCHAR(32) )')
      .then(() => {
        //   this.presentToast();
      })
      .catch(e => console.log(e));
      
      db.executeSql("SELECT * FROM cartllist WHERE prod_id=?", [data.id])
      .then(res => {
        console.log(res)
            if(res.rows.length==1) {
              console.log(res.rows.item(0).id)
              console.log(data.quantity)
                db.executeSql('UPDATE cartllist SET quantity=? WHERE id=?',[data.quantity, res.rows.item(0).id])
                .then(res => {
                  console.log(res);  
                  this.presentToast("Cart Updated");
                  this.getCartlength();
                })
                .catch(e => console.log("Update Sqlite"+e));
            }
            else{      
              //data insert section
              db.executeSql('INSERT INTO cartllist(prod_id, name, price, quantity, company, image) VALUES(\''+data.id+'\', \''+data.name+'\', \''+data.price+'\', \''+data.quantity+'\', \''+data.company+'\', \''+data.image+'\')', [])
              .then(() => {
                this.presentToast("Product Added to Cart");
                this.getCartlength();
              })
              .catch(e => console.log("Insert Sqlite"+e));
            }
          })

      .catch(e => console.log(e));
      })
      .catch(e => alert(JSON.stringify(e)));
  }

  goCart(){
    this.navCtrl.push("CartPage");
  }
}
