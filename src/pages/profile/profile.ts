import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApicallfunProvider } from '../../providers/apicall';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  data: any = {};
  orders: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private api: ApicallfunProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.data = JSON.parse(localStorage.getItem("iobasket"));
    this.getorderlist(this.data.id);
  }


  async getorderlist(id) {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please Wait...'
    });
    loading.present();
    var objsldr = {
      "user_id":id
    }
    console.log(objsldr);
    
    let orderlists = await this.api.orderlist(objsldr);
    if(orderlists.status==="1"){
      loading.dismiss();
      this.orders = orderlists.order_arr;
      this.data.prod_image_url = orderlists.prod_image_url;
      console.log(this.orders);
    }
    else{
      loading.dismiss();
    }
  }
}
