import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController  } from 'ionic-angular';
import { ApicallfunProvider } from '../../providers/apicall';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  data: any={};
  product: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private api: ApicallfunProvider, private toastCtrl: ToastController, private sqlite: SQLite) {
  }

  ionViewWillEnter(){
    this.getCartlength();
  }
  ionViewDidLoad() {
    this.data = JSON.parse(localStorage.getItem("iobasket"));;
    console.log('ionViewDidLoad SearchPage');
  }

  goCart(){
    this.navCtrl.push("CartPage");
  }

  async onSearch(data) {
    if(data==""){
    }
    else if(data.length>3){
      let serchData = {
        "user_id": this.data.id,
        "product_name": data
      }
      let search = await this.api.getsearchproduct(serchData);
      console.log(data);
      if(search.status==="1"){
        this.data.prod_image_url = search.prod_image_url
        this.product = search.product
        console.log(this.product);
        this.data.quantity = 1;
        for(var x = 0; x<this.product.length; x++ ){
          this.product[x].quantity = 1
          this.product[x].price = parseInt(this.product[x].price)
        }
      }
      else{
      }
    }
  }


  addtocart(id, data){
    console.log(id);
    console.log(data);

    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
      })
      .then((db: SQLiteObject) => {
      
      //create table section
      db.executeSql('CREATE TABLE IF NOT EXISTS cartllist(id INTEGER PRIMARY KEY AUTOINCREMENT, prod_id INTEGER, name VARCHAR(32), price INTEGER, quantity INTEGER, company VARCHAR(32), image VARCHAR(32) )')
      .then(() => {
        //   this.presentToast();
      })
      .catch(e => console.log(e));
      
      db.executeSql("SELECT * FROM cartllist WHERE prod_id=?", [data.id])
      .then(res => {
        console.log(res)
            if(res.rows.length==1) {
              console.log(res)
                db.executeSql('UPDATE cartllist SET quantity=? WHERE id=?',[data.quantity, data.id])
                .then(res => {
                  console.log(res);  
                  this.presentToast("Cart Updated");
                  this.getCartlength();
                })
                .catch(e => console.log("Update Sqlite"+e));
            }
            else{      
              //data insert section
              db.executeSql('INSERT INTO cartllist(prod_id, name, price, quantity, company, image) VALUES(\''+data.id+'\', \''+data.name+'\', \''+data.price+'\', \''+data.quantity+'\', \''+data.company+'\', \''+data.image+'\')', [])
              .then(() => {
                this.presentToast("Prodyct Added to Cart");
                this.getCartlength();
              })
              .catch(e => console.log("Insert Sqlite"+e));
            }
          })

      .catch(e => console.log(e));
      })
      .catch(e => alert(JSON.stringify(e)));
  }

  
  qtyplus(id, quantity){
    quantity = quantity+1
    this.product[id].quantity = quantity;
  }
  qtyminus(id, quantity){
    if(quantity>1){
      quantity = quantity-1
      this.product[id].quantity = quantity;
    }
  }
  
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  getCartlength(){

    this.sqlite.create({
      name: 'ioshopdata.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
    
      db.executeSql("SELECT * FROM cartllist", [])
      .then(res => {
        console.log("Selected Data from db"+res)
            if(res.rows.length>0) {
              this.data.cartlength = res.rows.length
              }
              else{
                this.data.cartlength = 0;
              }
          })

        })

  }

}
