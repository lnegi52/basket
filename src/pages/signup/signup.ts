import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicallfunProvider } from '../../providers/apicall';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  credentialsForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public formBuilder: FormBuilder, private api: ApicallfunProvider, public menu: MenuController) {
    this.credentialsForm = this.formBuilder.group({
      name: [''],
      email: [''],
      password: [''],
      mobileno: [''],
      address: [''],
      terms: [false]
    });
  }

  ionViewWillEnter(){
    this.menu.enable(false);
  }
  ionViewWillLeave(){
    this.menu.enable(true);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  async signUp() {
    console.log(this.credentialsForm.value)
    if(this.credentialsForm.value.name==""||this.credentialsForm.value.email==""||this.credentialsForm.value.password==""||this.credentialsForm.value.phone==""||this.credentialsForm.value.address==""){
      alert("All fields are mandatory");
    }
    else if(this.credentialsForm.value.terms==undefined||this.credentialsForm.value.terms==false){
      alert("Please Accept terms and condition.")
    }
    else{
      let loading = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Please Wait...'
      });
      loading.present();
      let signup = await this.api.getregister(this.credentialsForm.value);
      console.log(signup);
      localStorage.setItem("iootpId",signup.data.id)
      if(signup.status==="1"){
        loading.dismiss();
        let otpUrl = await this.api.getotp(signup.otp_url);
        if(otpUrl.status==="OK"){
          alert(otpUrl.message);
          this.navCtrl.setRoot("OtpPage");
        }
      }
      else{
        loading.dismiss();
        alert(signup.message);
      }
    }
  }

  onLogin(){
    this.navCtrl.setRoot("LoginPage");
  }
}
