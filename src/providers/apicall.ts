import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApicallProvider } from '../providers/api';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApicallfunProvider {
    static profilepicURl = "http://173.255.247.199/eazypo/public/uploads/images/";

    private baseURl = ApicallProvider.baseURl;

    constructor(private http: Http){
    }

    async getlogin(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"login", data).toPromise();
        return response.json();
    }
    async getregister(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"register", data).toPromise();
        return response.json();
    }
    async getforgotpass(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"forgotpassword",data).toPromise();
        return response.json();
    }
    async getotp(data): Promise<any> {
        const response = await this.http.get(data).toPromise();
        return response.json();
    }
    async getverifyotp(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"verifyotp", data).toPromise();
        return response.json();
    }
    async getslider(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"slider", data).toPromise();
        return response.json();
    }
    async getcategory(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"category", data).toPromise();
        return response.json();
    }
    getCategory2(data: any): Observable<any> {
        return this.http
          .post(this.baseURl+"category", data)
          .map(res => res.json());
      }
    async getsubcategory(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"subcategory", data).toPromise();
        return response.json();
    }
    async getproduct(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"productbysubcat", data).toPromise();
        return response.json();
    }
    async getsearchproduct(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"searchproduct", data).toPromise();
        return response.json();
    }
    async orderlist(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"orderlist", data).toPromise();
        return response.json();
    }
    async getplaceorder(data): Promise<any> {
        const response = await this.http.post(this.baseURl+"addorder", data).toPromise();
        return response.json();
    }


    
}